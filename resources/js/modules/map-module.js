
var config = {
  map: '#map',
  modal: '#myModal',
  pinElem: '.pin',
  counter: 0,
  pin: {
    pinHeight: 24,
    pinWidth: 24
  },
  pinList: [],
  form: {
    name: '.js-form-name',
    coox: '.js-coo-x',
    cooy: '.js-coo-y',
    save: '.js-save-pin',
    delete: '.js-delete-pin',
    show: '.js-show-all',
    reset: '.js-reset-all'
  },
  controlObject: {}
};

$(config.map).on('click', function(e) {

  var $target = $(e.target);
  var coo = getPinCoordinates(e);

  $(config.form.delete).hide();

  if( $target.hasClass('pin') ) {
    editCurrentPin($target, coo);
    return false;
  }

  resetModalValues();
  insertCoordinates(coo);
});

var editCurrentPin = function ($target, coo) {
  $(config.form.delete).show();
  insertCoordinates(coo);

  var id = $target.attr('data-pin-id');
  var data = config.controlObject[id];
  var cityName = $(config.form.name).val(data.name);

  $(config.form.delete).attr('data-current-id', id);

  $(config.modal).modal('show');
  $(config.form.delete).hide();
};

var getPinCoordinates = function (target) {
  var mouseX = target.pageX;
  var mouseY = target.pageY;

  return {
    x: mouseX,
    y: mouseY
  };
};

var resetModalValues = function () {
  $(config.form.name).val('');
};

var insertCoordinates = function (obj) {
   $(config.form.coox).val(obj.x);
   $(config.form.cooy).val(obj.y);
};

$(config.form.save).on('click', function () {
  var cityNameVal = $(config.form.name).val();
  var cityX = $(config.form.coox).val();
  var cityY = $(config.form.cooy).val();

  if( cityNameVal === ''){
    return false;
  }

  addPinController({
    cityNameVal: cityNameVal,
    cityX: cityX,
    cityY: cityY
  });

  $(config.modal).modal('hide');
});

var addPinController = function (obj) {

  /**
  * obj.ctiyNameVal {String}
  * obj.cityX {Number}
  * obj.cityy {Number}
  */

  pinControls.add({
    id: config.counter,
    x: obj.cityX,
    y: obj.cityY,
    name: obj.cityNameVal
  });

  var $pin = $('<span class="pin" data-toggle="tooltip" title='+obj.cityNameVal+'></span>');
  $('[data-toggle="tooltip"]').tooltip();


  $pin.attr('data-pin-id', config.counter);
  $pin.css({
    top: obj.cityY + 'px',
    left: obj.cityX + 'px'
  });

  config.counter++;

  $(config.map).append($pin);
}

$(config.form.delete).on('click', function () {
  var $this = $(this);
  var currentId = Number($this.attr('data-current-id'));
  var $currentPin = $(config.map).find('*[data-pin-id="'+currentId+'"]');

  pinControls.remove(currentId);
  $(config.modal).hide();
  $currentPin.remove();
});

$(config.form.show).on('click', function(){
  var $table = $('table').find('tbody');

  $table.empty();

  for (var property in config.controlObject) {
    if (config.controlObject.hasOwnProperty(property)) {
      var cityName = config.controlObject[property].name;
      var xCoo = config.controlObject[property].x;
      var yCoo = config.controlObject[property].y;
      $table.append('<tr><th class="font">'+cityName+'</th><th class="font">'+xCoo+'</th><th class="font">'+yCoo+'</th></tr>');
    }
  }

});

$(config.form.reset).on('click', function(){
  $(config.pinElem).remove();
  for (var property in config.controlObject){
    delete config.controlObject[property];
  }
});


var pinControls = {
  add: function (obj) {

    /**
    * obj.id {Number}
    * obj.x {Number}
    * obj.y {Number}
    * obj.name {String}
    */

    if($.inArray(obj.id, config.pinList) === -1){

      config.pinList.push(obj.id);

      config.controlObject[obj.id] = {
        name: obj.name,
        x: obj.x,
        y: obj.y,
        id: obj.id
      };
    }
  },
  remove: function (id) {
    var index = $.inArray(id, config.pinList);

    if(index !== -1){
      config.pinList.splice(index, 1);
      delete config.controlObject[id];
    }

  }
};

$.ajax({
  url: 'api/pins.json',
  type: 'GET',
  //data: 'twitterUsername=jquery4u',
  success: function(data) {
    var response = data;

    console.log(data.name);

    for (var property in response) {
      if (response.hasOwnProperty(property)) {
         addPinController({
           cityNameVal: response[property].name,
           cityX: response[property].x,
           cityY: response[property].y
         });
       }
    }
  },
  error: function(e) {
  	//called when there is an error
  	//console.log(e.message);
  }
});
