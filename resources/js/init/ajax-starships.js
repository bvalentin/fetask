var apiStarship = 'http://swapi.co/api/starships/';

var liTemplate =
'<li class="list-item">' +
  '<div class="list-content">' +
    '<div class="list-content__first">' +
      '<div class="imgholder">' +
        '<img src="{entityImage}" alt="{entityName}" class="img-card img-responsive">' +
      '</div>' +
      '<h3 class="card-user">{entityName}</h3>' +
      '<p class="text-user">For more info about <span class="inner-text">{entityName}</span> just click on <span class="inner-text">arrow</span>.</p>' +
      '<i class="fa fa-chevron-down fa-lg  show-value js-show-values" aria-hidden="true" data-url="{entityUrl}" data-loaded="false"></i>' +
    '</div>' +
    '<div class="list-content__second people-list" id="second">' +
      '<div class="info">' +
        '<div class="info-about"></div>' +
      '</div>' +
    '</div>' +
  '</div>' +
'</li>';


var parseTemplate = function (templateString, templateData) {
  for (var p in templateData)
    templateString = templateString.replace(new RegExp('{' + p + '}', 'g'), templateData[p]);
  return templateString;
};

var displayShip = function(shipData) {
    var templateShip = parseTemplate(liTemplate, {
      entityImage: 'dist/images/ship.jpg',
      entityUrl: shipData.url,
      entityName: shipData.name
    });
    $('.js-ships-container').append(templateShip);
};

var load11 = false;

var getJsonDataShip = function(shipUrl, numberOfResultsShip) {
  $.getJSON(shipUrl, function( data ) {
    for (var j = 0; j < numberOfResultsShip; j++) {
      if(typeof data.results[j] !== 'undefined') {
        displayShip(data.results[j]);
      }
    }

    if(load11 === false) {
      getJsonDataShip(data.next, 1);
      load11 = true;
    }
  });
};
var loadShips = function() {
  $('.js-ships-container').empty();
  getJsonDataShip(apiStarship, 10);
};

loadShips();

$('.js-ships-container').on('click', '.js-show-values', function(){
  var $this = $(this);
  var $info = $this.closest('.list-item').find('.info-about');
  var loadedShip = $this.attr('data-loaded');

  $this.parent().next().toggleClass('active');

  if(loadedShip === 'false'){
    var shipApi = $this.attr('data-url');

    $.getJSON(shipApi, function(data){
      var parseDataShip = {};
      for (var key in data) {
        parseDataShip[key] = {
          label: key.split('_').join(' '),
          value: data[key]
        };
      }

      $info.append('<div class="key-value">'+'<p class="bold">'+parseDataShip.name.value+'</p>' + '<p class="key">'+parseDataShip.name.label+'</p>'+'</div>');
      $info.append('<div class="key-value">'+'<p class="bold">'+parseDataShip.model.value+'</p>' + '<p class="key">'+parseDataShip.model.label+'</p>'+'</div>');
      $info.append('<div class="key-value">'+'<p class="bold">'+parseDataShip.max_atmosphering_speed.value+'</p>' + '<p class="key">'+parseDataShip.max_atmosphering_speed.label+'</p>'+'</div>');
      $info.append('<div class="key-value">'+'<p class="bold">'+parseDataShip.manufacturer.value+'</p>' + '<p class="key">'+parseDataShip.manufacturer.label+'</p>'+'</div>');
      $info.append('<div class="key-value">'+'<p class="bold">'+parseDataShip.passengers.value+'</p>' + '<p class="key">'+parseDataShip.passengers.label+'</p>'+'</div>');
      $info.append('<div class="key-value">'+'<p class="bold">'+parseDataShip.cost_in_credits.value+'</p>' + '<p class="key">'+parseDataShip.cost_in_credits.label+'</p>'+'</div>');
      $info.append('<div class="key-value">'+'<p class="bold">'+parseDataShip.crew.value+'</p>' + '<p class="key">'+parseDataShip.crew.label+'</p>'+'</div>');
      $info.append('<div class="key-value">'+'<p class="bold">'+parseDataShip.consumables.value+'</p>' + '<p class="key">'+parseDataShip.consumables.label+'</p>'+'</div>');

      $this.attr('data-loaded', true);
    });
  }

});
