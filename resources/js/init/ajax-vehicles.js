var apiVehicles = 'http://swapi.co/api/vehicles/';

var liTemplate =
'<li class="list-item">' +
  '<div class="list-content">' +
    '<div class="list-content__first">' +
      '<div class="imgholder">' +
        '<img src="{entityImage}" alt="{entityName}" class="img-card img-responsive">' +
      '</div>' +
      '<h3 class="card-user">{entityName}</h3>' +
      '<p class="text-user">For more info about <span class="inner-text">{entityName}</span> just click on <span class="inner-text">arrow</span>.</p>' +
      '<i class="fa fa-chevron-down fa-lg  show-value js-show-values" aria-hidden="true" data-url="{entityUrl}" data-loaded="false"></i>' +
    '</div>' +
    '<div class="list-content__second people-list" id="second">' +
      '<div class="info">' +
        '<div class="info-about"></div>' +
      '</div>' +
    '</div>' +
  '</div>' +
'</li>';


var parseTemplate = function (templateString, templateData) {
  for (var p in templateData)
    templateString = templateString.replace(new RegExp('{' + p + '}', 'g'), templateData[p]);
  return templateString;
};

var displayVehicle = function(vehicleData){
  var templateVehicle = parseTemplate(liTemplate, {
    entityImage: 'dist/images/vehicle.jpg',
    entityUrl: vehicleData.url,
    entityName: vehicleData.name
  });
  $('.js-vehicle-container').append(templateVehicle);
};

var loadVehicles11 = false;

var getJsonDataVehicle = function(vehicleUrl, numberOfResultsVehicle){
  $.getJSON(vehicleUrl, function(data) {
    for(var k = 0; k < numberOfResultsVehicle; k++){
      if(typeof data.results[k] !== 'undefined'){
        displayVehicle(data.results[k]);
      }
    }

    if(loadVehicles11 === false){
      getJsonDataVehicle(data.next, 1);
      loadVehicles11 = true;
    }

  });
};

var loadVehicles = function () {
  $('.js-ships-container').empty();
  getJsonDataVehicle(apiVehicles, 10);
};

loadVehicles();

$('.js-vehicle-container').on('click', '.js-show-values', function(){
  var $this = $(this);
  var $info = $this.closest('.list-item').find('.info-about');
  var loadedVehicle = $this.attr('data-loaded');

  $this.parent().next().toggleClass('active');

  if(loadedVehicle === 'false'){
    var vehicleApi = $this.attr('data-url');

    $.getJSON(vehicleApi, function(data){
      var parseDataVehicle = {};
      for(var key in data){
        parseDataVehicle[key] = {
          label: key.split('_').join(' '),
          value: data[key]
        };
      }

      $info.append('<div class="key-value">'+'<p class="bold">'+parseDataVehicle.name.value+'</p>' + '<p class="key">'+parseDataVehicle.name.label+'</p>'+'</div>');
      $info.append('<div class="key-value">'+'<p class="bold">'+parseDataVehicle.model.value+'</p>' + '<p class="key">'+parseDataVehicle.model.label+'</p>'+'</div>');
      $info.append('<div class="key-value">'+'<p class="bold">'+parseDataVehicle.max_atmosphering_speed.value+'</p>' + '<p class="key">'+parseDataVehicle.max_atmosphering_speed.label+'</p>'+'</div>');
      $info.append('<div class="key-value">'+'<p class="bold">'+parseDataVehicle.manufacturer.value+'</p>' + '<p class="key">'+parseDataVehicle.manufacturer.label+'</p>'+'</div>');
      $info.append('<div class="key-value">'+'<p class="bold">'+parseDataVehicle.passengers.value+'</p>' + '<p class="key">'+parseDataVehicle.passengers.label+'</p>'+'</div>');
      $info.append('<div class="key-value">'+'<p class="bold">'+parseDataVehicle.cost_in_credits.value+'</p>' + '<p class="key">'+parseDataVehicle.cost_in_credits.label+'</p>'+'</div>');
      $info.append('<div class="key-value">'+'<p class="bold">'+parseDataVehicle.crew.value+'</p>' + '<p class="key">'+parseDataVehicle.crew.label+'</p>'+'</div>');
      $info.append('<div class="key-value">'+'<p class="bold">'+parseDataVehicle.consumables.value+'</p>' + '<p class="key">'+parseDataVehicle.consumables.label+'</p>'+'</div>');


      $this.attr('data-loaded', true);
    });
  }
});
