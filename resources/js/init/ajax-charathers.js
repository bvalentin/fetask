var sWApi = 'http://swapi.co/api/people/';

var liTemplate =
'<li class="list-item">' +
  '<div class="list-content">' +
    '<div class="list-content__first">' +
      '<div class="imgholder">' +
        '<img src="{entityImage}" alt="{entityName}" class="img-card img-responsive">' +
      '</div>' +
      '<h3 class="card-user">{entityName}</h3>' +
      '<p class="text-user">For more info about <span class="inner-text">{entityName}</span> just click on <span class="inner-text">arrow</span>.</p>' +
      '<i class="fa fa-chevron-down fa-lg  show-value js-show-values" aria-hidden="true" data-url="{entityUrl}" data-loaded="false"></i>' +
    '</div>' +
    '<div class="list-content__second people-list" id="second">' +
      '<div class="info">' +
        '<div class="info-about"></div>' +
      '</div>' +
    '</div>' +
  '</div>' +
'</li>';

var parseTemplate = function (templateString, templateData) {
  for (var p in templateData)
    templateString = templateString.replace(new RegExp('{' + p + '}', 'g'), templateData[p]);
  return templateString;
};

var displayCharacter = function(characterData) {
    var template = parseTemplate(liTemplate, {
      entityImage: 'dist/images/luke.jpg',
      entityUrl: characterData.url,
      entityName: characterData.name
    });
    $('.js-people-container').append(template);
};




var loaded11 = false;


var getJsonData = function(url, numberOfResults) {
  $.getJSON(url, function( data ) {
    for (var i = 0; i < numberOfResults; i++) {
      if(typeof data.results[i] !== 'undefined') {
        displayCharacter(data.results[i]);
      }
    }

    if(loaded11 === false) {
      getJsonData(data.next, 1);
      loaded11 = true;
    }
  });
};

var loadCharacters = function() {
  $('.js-people-container').empty();
  getJsonData(sWApi, 10);
};

loadCharacters();


$('.js-people-container').on('click', '.js-show-values', function(e){
  var $this = $(this);
  var $info = $this.closest('.list-item').find('.info-about');
  var loaded = $this.attr('data-loaded');

  $this.parent().next().toggleClass('active');

  if(loaded === 'false'){
    var peopleApi = $this.attr('data-url');

    // setTimeout(function(){
      $.getJSON( peopleApi, function( data ) {
        var parseData = {};
        for (var key in data) {
          parseData[key] = {
            label: key.split('_').join(' '),
            value: data[key]
          };
        }

        $info.append('<div class="key-value">'+'<p class="bold">'+parseData.name.value+'</p>' + '<p class="key">'+parseData.name.label+'</p>'+'</div>');
        $info.append('<div class="key-value">'+'<p class="bold">'+parseData.height.value+'</p>' + '<p class="key">'+parseData.height.label+'</p>'+'</div>');
        $info.append('<div class="key-value">'+'<p class="bold">'+parseData.mass.value+'</p>' + '<p class="key">'+parseData.mass.label+'</p>'+'</div>');
        $info.append('<div class="key-value">'+'<p class="bold">'+parseData.hair_color.value+'</p>' + '<p class="key">'+parseData.hair_color.label+'</p>'+'</div>');
        $info.append('<div class="key-value">'+'<p class="bold">'+parseData.skin_color.value+'</p>' + '<p class="key">'+parseData.skin_color.label+'</p>'+'</div>');
        $info.append('<div class="key-value">'+'<p class="bold">'+parseData.eye_color.value+'</p>' + '<p class="key">'+parseData.eye_color.label+'</p>'+'</div>');
        $info.append('<div class="key-value">'+'<p class="bold">'+parseData.birth_year.value+'</p>' + '<p class="key">'+parseData.birth_year.label+'</p>'+'</div>');
        $info.append('<div class="key-value">'+'<p class="bold">'+parseData.gender.value+'</p>' + '<p class="key">'+parseData.gender.label+'</p>'+'</div>');


        $this.attr('data-loaded', true);
    });
  // }, 1);

  }


});

// $('.js-people-container').on('click', '.js-close-info', function(e){
//     $(this).closest('.list-content__second').removeClass('active');
// });
