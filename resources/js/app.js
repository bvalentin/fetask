
/*INCLUDES FROM node_modules*/

//=include ../../node_modules/jquery/dist/jquery.min.js



//=include ../../node_modules/bootstrap/js/alert.js
//=include ../../node_modules/bootstrap/js/collapse.js
//=include ../../node_modules/bootstrap/js/tooltip.js
//=include ../../node_modules/bootstrap/js/popover.js
//=include ../../node_modules/bootstrap/js/dropdown.js
//=include ../../node_modules/bootstrap/js/modal.js



/*INCLUDES FROM resources*/

//=include init/ajax-charathers.js
//=include init/ajax-starships.js
//=include init/ajax-vehicles.js
//=include init/init.js
